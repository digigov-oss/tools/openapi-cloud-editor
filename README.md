# openapi cloud editor

A swagger editor that rocks. Can save the editing swagger file server-side and also protect the editor with a password.

## Build
`docker build -t openapicloudeditor:latest .`

## Run

`docker run --rm -d -e BASICAUTH="editor:password" -v ${PWD}/swagger.yaml:/swagger.yaml -p 8631:8631 openapicloudeditor`

* you can override the file name of swagger.yaml using the enviroment5 variable `SWAGGERFILE`

