import {Options,edit as openApiEditor} from 'openapi-editor';

if (process.env.SWAGGERFILE) {
const options:Options = {
  file: process.env.SWAGGERFILE , // specify path as string or fully resolved path
  host: '0.0.0.0', // specify ip 
  port: "8631", // specify port or omit for random port usage
  silent: true, // invoque browser or run silently
};
if (process.env.BASICAUTH) options.basicAuth = process.env.BASICAUTH

openApiEditor(options);

} else {
  console.error("No swagger file specified");
}