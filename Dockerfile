FROM node:16
ENV SWAGGERFILE=/swagger.yaml
EXPOSE 8631
COPY ./package.json /package.json
COPY editor.ts /editor.ts
COPY swagger.yaml /swagger.yaml
RUN yarn install
#need to do that cause ive get the openapi-editor from external repo
RUN cd node_modules/openapi-editor&&yarn install&&yarn build&&cd /
CMD ["yarn", "start"]
